<?php

/**
 * @file
 * Contains admin area menu callbacks and forms.
 */

/**
 * Form that asks user confirmation for order refund.
 *
 * @param array $form
 * @param array $form_state
 * @param object $order
 * @param object $transaction
 *
 * @return array
 */
function commerce_avangard_transaction_refund_form($form, &$form_state, $order, $transaction) {
  $form_state['order'] = $order;
  $form_state['transaction'] = $transaction;

  $transactions_balances = _commerce_avangard_get_order_transactions_balances($order, $transaction->remote_id);
  $amount_to_refund = $transactions_balances[$transaction->remote_id]['balance'];
  $form_state['amount'] = $amount_to_refund;

  $form['#submit'][] = 'commerce_avangard_transaction_refund_form_submit';

  $form = confirm_form($form,
    t('Are you sure you want to refund this transaction?'),
    "admin/commerce/orders/{$order->order_id}/payment",
    '<p>' . t('@amount paid via Avangard on @date.', array(
      '@amount' => commerce_currency_format($amount_to_refund, $transaction->currency_code),
      '@date' => format_date($transaction->created, 'short')
    )) . '</p>',
    t('Refund'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Implements hook_FORM_ID_form_submit().
 */
function commerce_avangard_transaction_refund_form_submit($form, &$form_state) {
  $result = commerce_avangard_transaction_refund($form_state['order'], $form_state['transaction'], $form_state['amount']);
  if ($result) {
    drupal_set_message(t('Transaction refunded successfully'));
    $form_state['redirect'] = "admin/commerce/orders/{$form_state['order']->order_id}/payment";
  }
}

/**
 * Displays list of transactions for the given date (uses today as default date).
 *
 * @param string $date
 *
 * @return string
 */
function commerce_avangard_date_operations($date = '') {
  if ($date === '') {
    $date = date('Y-m-d');
  }
  
  $build = array(
    '#prefix' => '<div id="operations-wrapper">',
    '#suffix' => '</div>',
  );

  if (module_exists('date_popup')) {
    $build['date_select'] = drupal_get_form('commerce_avangard_date_select_form', $date);
  }

  if ($date > date('Y-m-d')) {
    drupal_set_message(t('Future date transactions inquiries are not supported.'), 'warning');
    return $build;
  }
  
  $dtime = DateTime::createFromFormat('Y-m-d', $date);
  $response = _commerce_avangard_order_get_date_operations_list(date('d.m.Y', $dtime->getTimestamp()));

  if (isset($response['response_code']) && $response['response_code'] == 0 && !isset($response['oper_info'])) {
    drupal_set_message(t('No transactions found for selected date.'));
  }
  elseif (isset($response['oper_info'])) {
    $bank_operations = isset($response['oper_info']['id']) ? array($response['oper_info']) : $response['oper_info'];

    // Here we suppose that all transactions are made in site's default currency.
    $currency = commerce_default_currency();
    $rows = array_map(function ($operation) use($currency) {
      $link = "/admin/commerce/orders/{$operation['order_number']}/payment";
      return array(
        $operation['id'],
        "<a href='{$link}'>{$operation['order_number']}</a>",
        $operation['status_date'],
        commerce_currency_format($operation['amount'], $currency),
        $operation['status_code'],
        $operation['status_desc'],
        $operation['ticket'],
        isset($operation['card_num']) ? "{$operation['card_num']} ({$operation['exp_mm']}/{$operation['exp_yy']})" : '',
      );
    }, $bank_operations);
    $header = array(
      t('Id'),
      t('Order Id'),
      t('Time'),
      t('Sum'),
      t('Status code'),
      t('Status'),
      t('Ticket'),
      t('Extra'),
    );
    $caption = t('All transactions for selected date: !date', array('!date' => $date));

    $build['table']['#markup'] = theme('table', array(
      'rows' => $rows,
      'header' => $header,
      'caption' => $caption,
    ));
  }
  else {
    $error_message = t('Error occurred during communication with bank');
    if (isset($response['response_code']) && $response['response_code'] != 0 && isset($response['response_message'])) {
      $error_message = $response['response_message'];
    }
    elseif (isset($response['error'])) {
      $error_message = $response['error'];
    }
    drupal_set_message($error_message, 'error');
  }

  return $build;
}

function commerce_avangard_date_select_form($form, $form_state, $date) {
  $form['date'] = array(
    '#type' => 'date_popup',
    '#title' => t('Transactions date'),
    '#default_value' => $date,
    '#date_type' => DATE_DATETIME,
    '#date_timezone' => date_default_timezone(),
    '#date_format' => 'd-m-Y',
    '#date_increment' => 1,
    '#date_year_range' => '-3:+1',
    '#date_label_position' => 'invisible',
    '#ajax' => array(
      'callback' => 'commerce_avangard_date_select_form_submit',
      'wrapper' => 'operations-wrapper',
      'method' => 'replace',
    ),
  );

  return $form;
}

function commerce_avangard_date_select_form_submit($form, $form_state) {
  $date = $form_state['values']['date'];
  return commerce_avangard_date_operations($date);
}