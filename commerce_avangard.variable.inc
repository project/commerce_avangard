<?php

/**
 * @file
 * Lists module's variables.
 */

/**
 * Implements hook_group_info().
 */
function commerce_avangard_group_info() {
  $groups = array();

  $groups['commerce_avangard_main_settings'] = array(
    'title' => t('Main Avangard payment settings'),
    'access' => 'configure commerce avangard',
  );
  $groups['commerce_avangard_order_tokens'] = array(
    'title' => t('Order info tokens settings'),
    'access' => 'configure commerce avangard',
  );

  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function commerce_avangard_variable_info() {
  $variables = array();

  $variables['commerce_avangard_merchant_id'] = array(
    'title' => t('Merchant ID'),
    'description' => t('This is the merchant ID that Avangard sent you when you set up your account.'),
    'group' => 'commerce_avangard_main_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 10,
      '#required' => TRUE,
    ),
    'default' => '',
  );
  $variables['commerce_avangard_hash_password'] = array(
    'title' => t('Password'),
    'description' => t('This is the hash password that Avangard sent you.'),
    'group' => 'commerce_avangard_main_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 10,
      '#required' => TRUE,
    ),
    'default' => '',
  );
  $variables['commerce_avangard_server_timeout'] = array(
    'title' => t('Server connection timeout'),
    'description' => t('Time to wait till bank server respond to API request.'),
    'group' => 'commerce_avangard_main_settings',
    'type' => 'number',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 3,
      '#field_suffix' => 'sec',
    ),
    'default' => 60,
  );
  $variables['commerce_avangard_payment_process_timeout'] = array(
    'title' => t('Time to wait till customer finishes payments'),
    'description' => t('If customer finished payment process but never returned to the site we keep checking bank if it has information about payment during this period.'),
    'group' => 'commerce_avangard_main_settings',
    'type' => 'number',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 2,
      '#field_suffix' => 'hours',
      '#rules' => array('numeric'),
    ),
    'default' => 24,
  );
  $variables['commerce_avangard_log_api_requests'] = array(
    'title' => t('Log all API requests in watchdog for debugging purposes.'),
    'description' => t("Each time module makes request to API regarding order's state, it can log request's info to watchdog."),
    'group' => 'commerce_avangard_main_settings',
    'type' => 'boolean',
    'element' => array(
        '#type' => 'checkbox',
    ),
    'default' => FALSE,
  );
  $variables['commerce_avangard_back_url_query'] = array(
    'title' => t('Bank payment back URL additional query'),
    'description' => t('Sessions in analytics systems may break when user moves from your site to bank page. You may want to add special query to the url when user returns from bank page to prevent session break (like "?utm_nooverride=1").'),
    'group' => 'commerce_avangard_main_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 30,
    ),
    'default' => '',
  );

  $variables['commerce_avangard_tokens_client_name'] = array(
    'title' => t('Name'),
    'group' => 'commerce_avangard_order_tokens',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:commerce-customer-billing:commerce-customer-address:name-line]',
  );
  $variables['commerce_avangard_tokens_client_phone'] = array(
    'title' => t('Phone'),
    'group' => 'commerce_avangard_order_tokens',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:commerce-customer-billing:field-customer-phone]',
  );
  $variables['commerce_avangard_tokens_client_address'] = array(
    'title' => t('Address'),
    'group' => 'commerce_avangard_order_tokens',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:commerce-customer-billing:commerce-customer-address:locality] - [commerce-order:commerce-customer-billing:commerce-customer-address:thoroughfare]',
  );
  $variables['commerce_avangard_tokens_client_email'] = array(
    'title' => t('Email'),
    'group' => 'commerce_avangard_order_tokens',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => '[commerce-order:mail]',
  );
  $variables['commerce_avangard_tokens_order_description'] = array(
    'title' => t('Order description'),
    'group' => 'commerce_avangard_order_tokens',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
      '#maxlength' => 256,
    ),
    'default' => 'site payment',
  );
  $variables['commerce_avangard_tokens_language'] = array(
    'title' => t('Email'),
    'group' => 'commerce_avangard_order_tokens',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'default' => 'RU',
  );
  
  return $variables;
}
